﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Pipes;
using System.Security.Principal;
using System.Diagnostics;
using System.Threading;

namespace NamedPipeClientCsharp
{

    public class PipeClient
    {
        private const String PIPE_NAME = "mynamedpipe";
        private const string FILE_PATH = "text.txt";
        private static int numClients = 1;

        public static void Main(string[] Args)
        {
            if (Args.Length > 0)
            {
                if (Args[0] == "spawnclient")
                {
                    NamedPipeClientStream pipeClient =
                        new NamedPipeClientStream(".",
                            PIPE_NAME,
                            PipeDirection.InOut,
                            PipeOptions.None,
                            TokenImpersonationLevel.Impersonation);

                    Console.WriteLine("Connecting to server...\n");
                    pipeClient.Connect();

                    using (BinaryReader br = new BinaryReader(File.Open(FILE_PATH, FileMode.Open)))
                    {
                        byte[] data = ReadAllBytes(br);
                        pipeClient.Write(data, 0, data.Length);
                    }

                    pipeClient.Close();
                    // Give the client process some time to display results before exiting.
                    Thread.Sleep(2000);
                }
            }
            else
            {
                StartClients();
            }
        }

        private static byte[] ReadAllBytes(BinaryReader reader)
        {
            const int bufferSize = 4096;
            using (var ms = new MemoryStream())
            {
                byte[] buffer = new byte[bufferSize];
                int count;
                while ((count = reader.Read(buffer, 0, buffer.Length)) != 0)
                    ms.Write(buffer, 0, count);
                return ms.ToArray();
            }

        }

        // Helper function to create pipe client processes
        private static void StartClients()
        {
            int i;
            string currentProcessName = Environment.CommandLine;
            Process[] plist = new Process[numClients];

            Console.WriteLine("\n*** Named pipe client stream with impersonation example ***\n");
            Console.WriteLine("Spawning client processes...\n");

            if (currentProcessName.Contains(Environment.CurrentDirectory))
            {
                currentProcessName = currentProcessName.Replace(Environment.CurrentDirectory, String.Empty);
            }

            // Remove extra characters when launched from Visual Studio
            currentProcessName = currentProcessName.Replace("\\", String.Empty);
            currentProcessName = currentProcessName.Replace("\"", String.Empty);

            for (i = 0; i < numClients; i++)
            {
                // Start 'this' program but spawn a named pipe client.
                plist[i] = Process.Start(currentProcessName, "spawnclient");
            }
            while (i > 0)
            {
                for (int j = 0; j < numClients; j++)
                {
                    if (plist[j] != null)
                    {
                        if (plist[j].HasExited)
                        {
                            Console.WriteLine("Client process[{0}] has exited.",
                                plist[j].Id);
                            plist[j] = null;
                            i--;    // decrement the process watch count
                        }
                        else
                        {
                            Thread.Sleep(250);
                        }
                    }
                }
            }
            Console.WriteLine("\nClient processes finished, exiting.");
        }
    }
   
}