﻿// NamedPipeMarsClientCsharp.cs: main project file.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Pipes;
using System.Security.Principal;
using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices;

namespace NamedPipeMarsClientCsharp
{
    // The object which will be passed via a named pipe
    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Ansi)]
    public struct MyObject
    {
        [MarshalAs(UnmanagedType.I1)]
        public byte byteVal;
        [MarshalAs(UnmanagedType.I4)]
        public int intVal1;
        [MarshalAs(UnmanagedType.I4)]
        public int intVal2;

        public String toString()
        {
            return "MyObject: byteVal: " + this.byteVal + " intVal1: " + this.intVal1 + " intVal2: " + this.intVal2;
        }
    }

    public class PipeClient
    {
        private const String PIPE_NAME = "mynamedpipe";
        private static int numClients = 1;

        public static void Main(string[] Args)
        {
            if (Args.Length > 0)
            {
                if (Args[0] == "spawnclient")
                {
                    NamedPipeClientStream pipeClient =
                        new NamedPipeClientStream(".",
                            PIPE_NAME,
                            PipeDirection.InOut,
                            PipeOptions.None,
                            TokenImpersonationLevel.Impersonation);

                    Console.WriteLine("Connecting to server...\n");
                    pipeClient.Connect();

                    MyObject obj = CreateMyObject();
                    Console.WriteLine(obj.toString());
                    byte[] data = SerializeMyObject(obj);

                    pipeClient.Write(data, 0, data.Length);

                    pipeClient.Close();
                    // Give the client process some time to display results before exiting.
                    Thread.Sleep(2000);
                }
            }
            else
            {
                StartClients();
            }
        }

        // Creates a customized dummy instance of the object MyObject
        // It will be instantiated with arbitrary values and should just
        // demonstrates that all the values will be passed properly
        private static MyObject CreateMyObject()
        {
            MyObject obj = new MyObject();
            obj.byteVal = byte.MaxValue;
            obj.intVal1 = 5;
            obj.intVal2 = 42;
            return obj;
        }

        // Serializes an instance of MyObject into an array of bytes
        // so that it can be passed via a named pipe.
        private static byte[] SerializeMyObject(MyObject obj)
        {
            //get the size of the filled structure 
            int size = Marshal.SizeOf(obj);
            //allocate the byte array to write the structure into
            byte[] outBuffer = new byte[size];
            // Initialize unmanged memory to hold the struct.
            IntPtr pnt = Marshal.AllocHGlobal(size);
            // Copy the struct to unmanaged memory.
            Marshal.StructureToPtr(obj, pnt, false);
            Marshal.Copy(pnt, outBuffer, 0, size);
            Marshal.FreeHGlobal(pnt);

            return outBuffer;
        }

        // Helper function to create pipe client processes
        private static void StartClients()
        {
            int i;
            string currentProcessName = Environment.CommandLine;
            Process[] plist = new Process[numClients];

            Console.WriteLine("\n*** Named pipe client stream with impersonation example ***\n");
            Console.WriteLine("Spawning client processes...\n");

            if (currentProcessName.Contains(Environment.CurrentDirectory))
            {
                currentProcessName = currentProcessName.Replace(Environment.CurrentDirectory, String.Empty);
            }

            // Remove extra characters when launched from Visual Studio
            currentProcessName = currentProcessName.Replace("\\", String.Empty);
            currentProcessName = currentProcessName.Replace("\"", String.Empty);

            for (i = 0; i < numClients; i++)
            {
                // Start 'this' program but spawn a named pipe client.
                plist[i] = Process.Start(currentProcessName, "spawnclient");
            }
            while (i > 0)
            {
                for (int j = 0; j < numClients; j++)
                {
                    if (plist[j] != null)
                    {
                        if (plist[j].HasExited)
                        {
                            Console.WriteLine("Client process[{0}] has exited.",
                                plist[j].Id);
                            plist[j] = null;
                            i--;    // decrement the process watch count
                        }
                        else
                        {
                            Thread.Sleep(250);
                        }
                    }
                }
            }
            Console.WriteLine("\nClient processes finished, exiting.");
        }
    }
}
