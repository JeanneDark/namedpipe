﻿// NamedPipeCppCLRServer.cpp : main project file.

#include "stdafx.h"
#include <windows.h> 
#include <stdio.h> 
#include <tchar.h>
#include <strsafe.h>
#include <limits>

using namespace System;
using namespace System::Runtime::InteropServices;

DWORD WINAPI InstanceThread(LPVOID);

[StructLayout(LayoutKind::Sequential, Pack = 1, CharSet = CharSet::Ansi)]
ref struct MyObject
// The object which will be received as a data array then deserialized and
// then printed
{
	[MarshalAs(UnmanagedType::I1)]
	byte byteVal;
	[MarshalAs(UnmanagedType::I4)]
	int intVal1;
	[MarshalAs(UnmanagedType::I4)]
	int intVal2;

	String^ to_string()	// cannot evoke function in unmanaged code
	{
		return "MyObject: byteVal: " + byteVal + " intVal1: " + intVal1 + " intVal2: " + intVal2;
	}
};

MyObject^ DeserializeMyObject(char* data, int count)
// Deserializes a char array/ byte array into an instance of MyObject
// with all properties defined in the data array.
{
	int size = Marshal::SizeOf(MyObject::typeid);
	array<Byte>^ dataNew = gcnew array<Byte>(size);
	for (int i = 0; i < count; i++) {
		dataNew[i] = *(data + i);
	}

	GCHandle handle = GCHandle::Alloc(dataNew, GCHandleType::Pinned);
	MyObject^ obj = safe_cast<MyObject^>(Marshal::PtrToStructure(handle.AddrOfPinnedObject(), MyObject::typeid));

	return obj;
}

int main(array<System::String ^> ^args)
{
	BOOL   fConnected = FALSE;
	DWORD  dwThreadId = 0;										// windows unsigned int
	HANDLE hPipe = INVALID_HANDLE_VALUE, hThread = NULL;
	LPTSTR lpszPipename = TEXT("\\\\.\\pipe\\mynamedpipe");		// L‌ong P‌ointer to a C‌onst T‌CHAR STR‌ing (char*)

	// The main loop creates an instance of the named pipe and 
	// then waits for a client to connect to it. When the client 
	// connects, a thread is created to handle communications 
	// with that client, and this loop is free to wait for the
	// next client connect request. It is an infinite loop.

	for (;;)
	{
		_tprintf(TEXT("\nPipe Server: Main thread awaiting client connection on %s\n"), lpszPipename);
		// Creates an instance of a named pipe and returns a handle for subsequent pipe operations.
		// A named pipe server process uses this function either to create the first instance of a
		// specific named pipe and establish its basic attributes or to create a new instance of an existing named pipe.

		hPipe = CreateNamedPipe(
			lpszPipename,             // pipe name 
			PIPE_ACCESS_DUPLEX,       // read/write access // FILE_FLAG_OVERLAPPED
			PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT, // message type pipe, message-read mode, blocking mode
			PIPE_UNLIMITED_INSTANCES, // max. instances  
			INT_MAX,                  // output buffer size 
			INT_MAX,                  // input buffer size 
			0,                        // client time-out 
			NULL);                    // default security attribute 

		if (hPipe == INVALID_HANDLE_VALUE)
		{
			_tprintf(TEXT("CreateNamedPipe failed, GLE=%d.\n"), GetLastError());
			return -1;
		}

		// Wait for the client to connect; if it succeeds, 
		// the function returns a nonzero value. If the function
		// returns zero, GetLastError returns ERROR_PIPE_CONNECTED. 

		fConnected = ConnectNamedPipe(hPipe, NULL) ?
		TRUE : (GetLastError() == ERROR_PIPE_CONNECTED);

		if (fConnected)
		{
			printf("Client connected, creating a processing thread.\n");

			// Create a thread for this client. 
			hThread = CreateThread(
				NULL,				// no security attribute 
				0,					// default stack size 
				InstanceThread,		// thread proc
				(LPVOID)hPipe,		// thread parameter 
				0,					// not suspended 
				&dwThreadId);		// returns thread ID 

			if (hThread == NULL)
			{
				_tprintf(TEXT("CreateThread failed, GLE=%d.\n"), GetLastError());
				return -1;
			}
			else CloseHandle(hThread);
		}
		else
		{
			// The client could not connect, so close the pipe. 
			CloseHandle(hPipe);
		}
	}

	return 0;
}

DWORD WINAPI InstanceThread(LPVOID lpvParam)
// This routine is a thread processing function to read from and reply to a client
// via the open pipe connection passed from the main loop. Note this allows
// the main loop to continue executing, potentially creating more threads of
// of this procedure to run concurrently, depending on the number of incoming
// client connections.
{
	DWORD cbBytesRead = 0;
	BOOL fSuccess = FALSE;
	HANDLE hPipe = (HANDLE)lpvParam; // The thread's parameter is a handle to a pipe object instance.
	DWORD availBytes = 0;

	// Do some extra error checking since the app will keep running even if this
	// thread fails.

	if (lpvParam == NULL)
	{
		return (DWORD)-1;
	}

	// Print verbose messages. In production code, this should be for debugging only.
	printf("InstanceThread created, receiving and processing messages.\n");

	// Loop until done reading
	while (1)
	{
		// Use PeekNamedPipe because we don't know of what size the buffer is supposed to be
		fSuccess = PeekNamedPipe(
			hPipe,
			NULL,
			NULL,
			NULL,
			&availBytes,
			NULL);

		char* data = (char*)malloc(sizeof(char) * availBytes);

		fSuccess = ReadFile(
			hPipe,			// handle to pipe 
			data,			// buffer to receive data 
			availBytes,		// size of buffer 
			&cbBytesRead,	// number of bytes read 
			NULL);			// not overlapped I/O

		// Just for debugging purposes
		_tprintf(TEXT("Expected bytes: %d and bytes read: %d\n"), availBytes, cbBytesRead);

		if (!fSuccess && cbBytesRead == 0)
		{
			if (GetLastError() == ERROR_BROKEN_PIPE)
			{
				_tprintf(TEXT("InstanceThread: client disconnected.\n"), GetLastError());
			}
			else
			{
				_tprintf(TEXT("InstanceThread ReadFile failed, GLE=%d.\n"), GetLastError());
			}
			break;
		}
		else
		{
			MyObject^ obj = DeserializeMyObject(data, cbBytesRead);
			Console::WriteLine("MyObject: byteVal: " + obj->byteVal + " intVal1: " + obj->intVal1 + " intVal2: " + obj->intVal2);
		}

		if (!fSuccess)
		{
			_tprintf(TEXT("InstanceThread WriteFile failed, GLE=%d.\n"), GetLastError());
			break;
		}
	}

	// Flush the pipe to allow the client to read the pipe's contents 
	// before disconnecting. Then disconnect the pipe, and close the 
	// handle to this pipe instance. 

	FlushFileBuffers(hPipe);
	DisconnectNamedPipe(hPipe);
	CloseHandle(hPipe);

	printf("InstanceThread exitting.\n");
	return 1;
}